package com.tripagency;

public interface ITravelService {
	public void calculateTripPrice(Trip trip);
	public Iterable<TripData> getTrips();
}
