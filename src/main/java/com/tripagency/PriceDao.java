package com.tripagency;

public interface PriceDao {
	public int getTravelFees(Trip trip);
	public int getAgencyFees(Trip trip);
}
