package com.tripagency;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringBootWebTrip extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SpringBootWebTrip.class);
	}
	
    public static void main(String[] args) {
    	
        SpringApplication.run(SpringBootWebTrip.class, args);
    }

    @Bean
	public CommandLineRunner demo(TripRepository repository) {
		return (args) -> {
			// save a couple of customers
			repository.save(new TripData("Jack SMITH", "Paris"));
			repository.save(new TripData("Bobby SMITH", "London"));
			repository.save(new TripData("Laura SMITH", "Tokyo"));


		};
	}
}


