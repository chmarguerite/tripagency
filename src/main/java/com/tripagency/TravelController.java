package com.tripagency;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/")
public class TravelController {

	   @RequestMapping(value = { "/welcome"}, method = RequestMethod.GET)
		public String bienvenue(ModelMap model) {
	    	return "welcome";
	    }
	    
	   @RequestMapping(value = { "/trip"}, method = RequestMethod.GET)
		public String getTrip(ModelMap model) {
	    	return "trip";
	    }
	   
	   @RequestMapping(value = { "/calculate"}, method = RequestMethod.POST)
		public String postCalculateTrip(ModelMap model) {
	    	return "calculate";
	    }
	    
}
