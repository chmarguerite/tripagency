package com.tripagency;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

@Service
public class TravelService implements ITravelService {
	@Autowired(required=true)
	private TripPriceDao tripPriceDao;
	@Autowired(required=true)
	private TripRepository tripRepository;
	
	public TravelService() {
		super();
	}
	public TravelService(TripPriceDao tripPriceDao) {
		super();
		this.tripPriceDao = tripPriceDao;
	}
	
	public void setTripPriceDao(TripPriceDao tripPriceDao) {
		this.tripPriceDao = tripPriceDao;
	}
	
	public void calculateTripPrice(Trip trip) {
		trip.setPrice(tripPriceDao.getAgencyFees(trip) + tripPriceDao.getTravelFees(trip));	
	}

	public Iterable<TripData> getTrips() {
		// TODO Auto-generated method stub
		return tripRepository.findAll();
	}

}
