package com.tripagency;

public class Trip {
	private Destination destination;
	private int price;

	public Destination getDestination() {return destination;}
	public void setDestination(Destination destination) {this.destination = destination;}
	public int getPrice() {return price;}
	public void setPrice(int price) {this.price = price;}
	
}
