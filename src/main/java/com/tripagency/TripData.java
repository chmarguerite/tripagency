package com.tripagency;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="trip")
public class TripData {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String name;
	private String destination;
	private int price;
	protected TripData() {}

	public TripData(String name, String destination) {
		this.name = name;
	    this.destination = destination;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}

	    @Override
	    public String toString() {
	        return String.format(
	                "Trip[id=%d, name='%s', destination='%s', price=%]",
	                id, name, destination, price);
	    }
}
