package com.tripagency;

import org.springframework.stereotype.Repository;

@Repository
public class TripPriceDao implements PriceDao{

	public int getTravelFees(Trip trip) {
		 switch (trip.getDestination().getDestinationName()) {
         case "Paris":
             return 800;
         case "Lille":
             return 0;
         case "New-York":
         case "Tokyo":
        	 return 1000;
         case "Beijing":
             return 1500;
         default:
             return 0;
		 }
	}

	public int getAgencyFees(Trip trip) {
		 switch (trip.getDestination().getDestinationName()) {
         case "Paris":
             return 50;
         case "Lille":
             return 0;
         case "New-York":
         case "Tokyo":
        	 return 100;
         case "Beijing":
             return 150;
         default:
             return 0;
		 }
	}

}
