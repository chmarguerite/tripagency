package com.tripagency;

import org.springframework.data.repository.CrudRepository;

public interface TripRepository extends CrudRepository<TripData, Long> {

}
