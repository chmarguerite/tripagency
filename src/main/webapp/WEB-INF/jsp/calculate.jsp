<!DOCTYPE html>


<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html lang="en">
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- Access the bootstrap Css like this, 
		Spring boot will handle the resource mapping automcatically -->
	<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
	<!-- 
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
	<c:url value="/css/main.css" var="jstlCss" />
	<link href="${jstlCss}" rel="stylesheet" />
<script type="text/javascript">
$(document).ready(function() {
	  var ajaxData= $.ajax({
   	   async: false,
		   type: "GET",
		   dataType: "json",
		   url: "/api/v1/travelprice/${param.destination}" 
		 }).responseText;
		 var price = JSON.parse(ajaxData).price;
});
</script>
</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">BPI Service Qualification</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="trip">Trip</a></li>
					<li><a href="#about">About</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container">



<h2>Trip Informations</h2>
    <table id="tablePrice" border="1" class="table table-striped display">
            <thead>
                <tr>
                    <th>Destination</th>
                    <th>Passager</th>
                    <th>Prix</th>
                    <th>Commentaires</th>
                </tr>
            </thead>
            <tbody>
                    <tr>
                    <td><c:out value="${param.destination}" /></td>
                    <td><c:out value="${param.nom}" /></td>
                    <td><script>  var ajaxData= $.ajax({
                    	   async: false,
                		   type: "GET",
                		   dataType: "json",
                		   url: "/api/v1/travelprice/${param.destination}" 
                		 }).responseText;
                		 document.write(JSON.parse(ajaxData).price);</script></td>
                    <td><c:out value="${param.commentaires}" /></td>
                </tr>
                </tbody>
                 </table>
       

	</div>
	
	<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>

</html>