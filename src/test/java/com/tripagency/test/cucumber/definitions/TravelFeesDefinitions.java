package com.tripagency.test.cucumber.definitions;

import com.tripagency.Destination;
import com.tripagency.TravelService;
import com.tripagency.Trip;
import com.tripagency.TripPriceDao;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.mockito.Mockito;


public class TravelFeesDefinitions {
	Destination destination;
	Trip trip;
	TravelService travelPriceComputor;
	TripPriceDao tripPriceDao;
	
	@Given("^the customer want to travel to \"([^\"]*)\"$")
	public void the_customer_want_to_travel_to(String arg1) throws Throwable {
	    destination = new Destination(arg1);
	    trip = new Trip();
	    trip.setDestination(destination);
	    tripPriceDao = Mockito.mock(TripPriceDao.class);
	    travelPriceComputor = new TravelService(tripPriceDao);
	}

	@Given("^the travel fees are (\\d+)€$")
	public void the_travel_fees_are(int travelFees) throws Throwable {
		Mockito.when(tripPriceDao.getTravelFees(trip)).thenReturn(travelFees);
	}

	@Given("^the agency fees are (\\d+)€$")
	public void the_agency_fees_are(int agencyFees) throws Throwable {
		Mockito.when(tripPriceDao.getAgencyFees(trip)).thenReturn(agencyFees);
	}

	@When("^the system calculate the trip price$")
	public void the_system_calculate_the_trip_price() throws Throwable {
		travelPriceComputor.calculateTripPrice(trip);
	}

	@Then("^the trip price is (\\d+)€$")
	public void the_trip_price_is(int tripPrice) throws Throwable {
		Assert.assertEquals(tripPrice,trip.getPrice());
	}
}
