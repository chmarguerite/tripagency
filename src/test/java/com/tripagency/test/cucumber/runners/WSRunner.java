package com.tripagency.test.cucumber.runners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json"},
		features="src/test/resources/features/",
		glue = {"com.tripagency.test.webservices.definitions"})
public class WSRunner {}
