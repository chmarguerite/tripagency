package com.tripagency.test.webservices.definitions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tripagency.Trip;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Assert;

public class TravelFeesDefinitions {
	URL url;
	HttpURLConnection connection;
	
	@Before
	public void setup() throws MalformedURLException {	 
	}
	
	@Given("^the customer want to travel to \"([^\"]*)\"$")
	public void the_customer_want_to_travel_to(String arg1) throws Throwable {
		url = new URL("http://localhost:8080/api/v1/travelprice/" + arg1 );
	}

	@Given("^the travel fees are (\\d+)€$")
	public void the_travel_fees_are(int travelFees) throws Throwable {
		System.out.println("nothing to do");
	}

	@Given("^the agency fees are (\\d+)€$")
	public void the_agency_fees_are(int agencyFees) throws Throwable {
		System.out.println("nothing to do");
	}

	@When("^the system calculate the trip price$")
	public void the_system_calculate_the_trip_price() throws Throwable {
		connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.connect();
		
	}

	@Then("^the trip price is (\\d+)€$")
	public void the_trip_price_is(int tripPrice) throws Throwable {
		Assert.assertEquals(200,connection.getResponseCode());
		BufferedReader in = new BufferedReader(new InputStreamReader(
				connection.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Trip trip = gson.fromJson(response.toString(), Trip.class);
		Assert.assertEquals(tripPrice,trip.getPrice());

		
	}
}
